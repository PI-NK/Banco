use pinkdatabase;

create table if not exists Agendamento (
  cliente_id int not null,
  procedimento_id int not null,
  agenda_id int not null,
  `data` datetime not null,
  index fk_Clientes_has_Procedimentos_Procedimentos1_idx (procedimento_id ASC),
  index fk_Clientes_has_Procedimentos_Clientes_idx (cliente_id ASC),
  primary key (cliente_id, procedimento_id, agenda_id),
  constraint fk_Clientes_has_Procedimentos_Clientes
    foreign key (cliente_id)
    references Clientes (cliente_id)
    on delete no action
    on update no action,
  constraint fk_Clientes_has_Procedimentos_Procedimentos1
    foreign key (procedimento_id)
    references Procedimentos (procedimento_id)
    on delete no action
    on update no action);
    